aafigure (0.5-8) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * d/control: Fix wrong Vcs-*.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 16:57:36 -0500

aafigure (0.5-7) unstable; urgency=medium

  * Team upload.
  * Replacing python-imaging with python-pil (Closes: #866413).

 -- Stewart Ferguson <stew@ferg.aero>  Sun, 13 Jan 2019 09:33:29 +0100

aafigure (0.5-6) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/copyright: Changed licence shortname BSD -> BSD-3-clause
  * Bumped d/compat to 9
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/changelog: Remove trailing whitespaces
  * Convert git repository from git-dpm to gbp layout

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 20 Sep 2018 23:01:27 +0100

aafigure (0.5-5) unstable; urgency=low

  [ Stefano Rivera ]
  * Team upload.

  [ Jakub Wilk ]
  * Use fonts-liberation as an alternative recommendation to ttf-liberation.
  * Don't use dh_testdir; instead, use makefile rules to ensure that
    debian/rules can be only run in the correct directory.
  * Update years in debian/copyright.
  * Use canonical URIs for Vcs-* fields.

  [ Dimitri John Ledkov ]
  * Migrate to dh_python2 (Closes: #785945).

 -- Stefano Rivera <stefanor@debian.org>  Tue, 18 Aug 2015 17:19:19 +0200

aafigure (0.5-4) unstable; urgency=low

  * Bump version number of Debian Standards to  3.9.4
  * Set myself as maintainer (closes: #704925).

 -- "Super" Nathan Weber <supernathansunshine@gmail.com>  Mon, 13 May 2013 10:34:00 +0900

aafigure (0.5-3) unstable; urgency=low

  * Re-upload to Debian (closes: #669630).
  * Set myself as Maintainer and put Debian Python Modules Teams in Uploaders.
  * Bump standards version to 3.9.3.
    + Update debian/copyright URI.
  * Fix debian/copyright formatting.
  * Rewrite debian/rules from scratch, without using dh.
    + Reduce minimum required debhelper version to 7.
  * Add work-around for bug #645125.
  * Update patch headers.

 -- Jakub Wilk <jwilk@debian.org>  Mon, 23 Apr 2012 11:54:42 +0200

aafigure (0.5-2) unstable; urgency=low

  * QA upload (see #589967).
    + Set Maintainer to Debian QA Group.
    + Remove Uploaders.
  * Drop work-around for bug #575377.
  * Bump standards version to 3.9.2 (no changes needed).
  * Fix a typo in the manual page.
  * Relicense Debian packaging to the BSD license.
  * Simplify debian/rules. Add build-arch and build-indep targets.
  * Build-depend on python-all.
  * Update debian/copyright to the latest DEP-5 version.

 -- Jakub Wilk <jwilk@debian.org>  Thu, 16 Jun 2011 23:55:23 +0200

aafigure (0.5-1) unstable; urgency=low

  * New upstream release:
    + Drop patch to use Liberation fonts, no longer needed.
    + Enable font lookup code on non-Linux paltforms.
      [pilhelper-nonlinux.diff]
    + Update debian/copyright.
    + Update the manual page.
  * Bump standards version to 3.8.4 (no changes needed).
  * Switch to source format 3.0 (quilt).
  * Update my e-mail address.
  * Implement work-around for bug #575377.

 -- Jakub Wilk <jwilk@debian.org>  Thu, 25 Mar 2010 18:09:05 +0100

aafigure (0.4-2) unstable; urgency=low

  * debian/copyright: docutils/setup-docutils-plugin.py is in the public
    domain; thanks to Charles Plessy for spotting that.

 -- Jakub Wilk <ubanus@users.sf.net>  Fri, 17 Jul 2009 19:04:12 +0200

aafigure (0.4-1) unstable; urgency=low

  * Initial release (closes: #531922).
  * Use free Liberation fonts rather than Microsoft core fonts.

 -- Jakub Wilk <ubanus@users.sf.net>  Wed, 15 Jul 2009 20:28:33 +0200
